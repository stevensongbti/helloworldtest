package com.gbti.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	@ResponseBody
	public String institutionsScheduler(Model model) {
		return "Hello World !";
	}
}
