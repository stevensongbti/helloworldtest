package com.gbti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloworldtestApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloworldtestApplication.class, args);
	}
}
